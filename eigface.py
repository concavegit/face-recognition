#!/usr/bin/env python

'''
@author: Kawin Nikomborirak, Sreekanth Sajjala
@date: 2018/03/29

This script generates and saves the vectors needed to recognize faces
using eigenfaces.
'''

import numpy as np
from scipy.sparse.linalg import svds
from scipy.io import loadmat
from datetime import datetime

# begin measurements
startTime = datetime.now()

# load training data
d = loadmat('data/classdata_full')

# vectorize images
x = d['grayfaces'].reshape(-1, d['grayfaces'].shape[2])

# get names of images
y = np.reshape([str(x[0][:-7]) for x in d['y'][0][0][0][0]], (1, -1))
yUniq = np.unique(y)

# list of matrices of images indexed by person
cX = [np.empty((x.shape[0], 0)) for i in np.arange(yUniq.size)]

for i in np.arange(y.shape[1]):
    idx = np.where(yUniq == y[0][i])[0][0]
    cX[idx] = np.hstack((cX[idx], x[:, i:i + 1]))


# mean-center the faces
c = x - x.mean(1, keepdims=True)

# Use SVD to find the eigenfaces
ef = svds(c, 90)[0]

# transformation of training data onto eigenfaces
# cls = np.dot(ef.T, x)
cls = np.hstack([np.dot(ef.T, i).mean(1, keepdims=True) for i in cX])

# save the necessary vectors
np.save('data/eigcls', cls)
np.save('data/eignames', yUniq.reshape(1, -1))
np.save('data/ef', ef)

print(datetime.now() - startTime)
