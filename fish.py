#!/usr/bin/env python

'''
@author: Kawin Nikomborirak, Sreekanth Sajjala
@date: 2018/03/29

This script generates and saves the vectors needed to recognize faces
using fisherfaces.
'''

import numpy as np
from scipy.sparse.linalg import svds
from scipy.sparse.linalg import eigs
from scipy.io import loadmat
from datetime import datetime

# start memory and time measurements
startTime = datetime.now()

d = loadmat('data/classdata_full')

# vectorize images
x = d['grayfaces'].reshape(-1, d['grayfaces'].shape[2])

# Get a row vector of names from the struct
y = np.reshape([str(x[0][:-7]) for x in d['y'][0][0][0][0]], (1, -1))

# generate the list of unique names
yUniq = np.unique(y)
n = x.shape[1]

# the amount of eigenvectors to use
c = n - yUniq.size

# list of matrices of images indexed by person
cX = [np.empty((x.shape[0], 0)) for i in np.arange(yUniq.size)]

for i in np.arange(y.shape[1]):
    idx = np.where(yUniq == y[0][i])[0][0]
    cX[idx] = np.hstack((cX[idx], x[:, i:i + 1]))

# mean of x
muX = x.mean(1, keepdims=True)
mcX = x - muX

# use SVD to obtain the principle components uP.
uP = svds(mcX, c)[0]

# transpose uP to obtain the transformation matrix from X to P
wP = uP.T

# cX transformed to P space
cP = [np.dot(wP, i) for i in cX]

# x transformed to P space
p = np.dot(wP, x)
muP = p.mean(1, keepdims=True)

# matrix containing means in muP
muCP = np.hstack([i.mean(1, keepdims=True) for i in cP])
cPN = np.hstack([i.shape[1] for i in cP])

# find Sb and Sw
sBTmp = (muCP - muP) * np.sqrt(cPN)
sB = np.dot(sBTmp, sBTmp.T)
sWTmp = [i - i.mean(1, keepdims=True) for i in cP]
sW = sum([np.dot(i, i.T) for i in sWTmp])

# solve the general eigen solution and find the discriminant components
fv = eigs(np.dot(np.linalg.inv(sW), sB), yUniq.size - 1)[1]

# transform things from P or X to F space using wF and F respectively.
wF = fv.T
w = np.dot(wF, wP)

# generate cX in F space
cls = np.hstack([np.dot(wF, i).mean(1, keepdims=True) for i in cP])

# save the training results.
np.save('data/fishcls', cls)
np.save('data/fishw', w)
np.save('data/fishnames', yUniq.reshape(1, -1))

# print metrics
print(datetime.now() - startTime)
