import numpy as np
from scipy.io import loadmat

names = np.load('data/name.npy')
fnames = names.flatten()
fw = np.load('data/fishall.npy')
ew = np.load('data/eigall.npy')

t = loadmat('data/train')
tx = t['grayfaces'].reshape(-1, t['grayfaces'].shape[2])
ty = np.reshape([str(x[0][:-7]) for x in t['y'][0][0][0][0]], (1, -1))

c = [np.empty((tx.shape[0], 0)) for i in np.arange(names.size)]

for i in np.arange(ty.shape[1]):
    # idx = np.where(name == ty[0][i])[0][0]
    # c[idx] = np.hstack((c[idx], t[:, i:i + 1]))
    idx = np.where(fnames == ty[0][i])[0][0]
    c[idx] = np.hstack((c[idx], tx[:, i:i + 1]))


test = loadmat('data/test')
testx = test['grayfaces'].reshape(-1, test['grayfaces'].shape[2])
testy = np.reshape([str(x[0][:-7]) for x in test['y'][0][0][0][0]], (1, -1))


def checkFish(n):
    cls = np.hstack([np.dot(fw[:n], i).mean(1, keepdims=True) for i in c])
    p = np.dot(fw[:n], testx)

    cnt = 0
    for i in np.arange(p.shape[1]):
        dists = np.linalg.norm(cls - p[:, i:i + 1], axis=0)
        guess = names[:, dists.argmin()]
        ans = testy[:, i]
        cnt += guess == ans

    return cnt


def checkEig(n):
    cls = np.hstack([np.dot(ew[:n], i).mean(1, keepdims=True) for i in c])
    p = np.dot(ew[:n], testx)

    cnt = 0
    for i in np.arange(p.shape[1]):
        dists = np.linalg.norm(cls - p[:, i:i + 1], axis=0)
        guess = names[:, dists.argmin()]
        ans = testy[:, i]
        cnt += guess == ans

    return cnt


def checkBoth(n):
    return np.reshape([checkFish(n), checkEig(n)], (-1, 1))


res = np.reshape([], (2, 0))
for n in range(0, ew.shape[0]):
    res = np.hstack((res, checkBoth(n)))
    print(n)
